/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.mavenproject1;

import java.util.ArrayList;
import java.util.List;

import java.awt.Image;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.Writer;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;

import java.util.Scanner;
import javax.imageio.ImageIO;

/**
 *
 * @author ramanzes
 */
public class Mavenproject1 {

    public static void main(String[] args) throws IOException {
        System.out.print("Введите символ:");
    
    
    
        Scanner scanner = new Scanner(System.in);
        
//        String str = scanner.next();
//        
        InputStream x = System.in;
//        
//        System.out.println(str);
//        System.err.println("its ok");
//        System.out.println((char)x.read());
//
////        InputStream stream = new FileInputStream("/tmp/qwerty.txt");
//        InputStream stream2 = new FileInputStream("/home/ramanzes/myapp/synergy/1_9/NetBeans/tema1_urok1_9/mavenproject1/src/main/java/com/mycompany/mavenproject1/qwerty.txt");
//
////        Scanner filescan = new Scanner(stream);
//        Scanner filescan2 = new Scanner(stream2);
//        
////        System.err.println(filescan.nextLine());
//        System.err.println(filescan2.nextLine());
//        
//try(InputStream in = new URL("https://upload.wikimedia.org/wikipedia/commons/c/c5/JPEG_example_down.jpg").openStream()){
//    Files.copy(in, Paths.get("/tmp/image.jpg"));
//} catch (IOException e) {
//    System.err.println("ошибка"+e);
//    // handle IOException
//}
//        
//URL url = new URL("https://upload.wikimedia.org/wikipedia/commons/c/c5/JPEG_example_down.jpg");
//InputStream in = new BufferedInputStream(url.openStream());
//ByteArrayOutputStream out = new ByteArrayOutputStream();
//byte[] buf = new byte[1024];
//int n = 0;
//while (-1!=(n=in.read(buf)))
//{
//   out.write(buf, 0, n);
//}
//out.close();
//in.close();
//byte[] response = out.toByteArray();        
//        
//    
//    FileOutputStream fos = new FileOutputStream("/tmp/image.jpg");
//fos.write(response);
//fos.close();
    
//    try (
//            URL url = new URL("http://images.app.goo.gl/Bhm6GUBahMSe5PyU9")) 
//    {
//    Image image = ImageIO.read(url);
//} catch (IOException e) {
//    // handle IOException
//}

////Урок 9. Ввод
////-
//вывод
//Цель задания: 
//Знакомство с базовым вводом 
//-
//выводом данных в java
//Задание: 
//Пользователь вводит три слова, выведите в обратном порядке.
//1.

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!! 1 !!!!!!!!!!!!!!!!!!!!!!!!!!!");
        System.out.println("Пользователь вводит три слова:");
        String w1=scanner.next();
        String w2=scanner.next();
        String w3=scanner.next();
        System.out.println("вывод:");
        System.out.println(w3);
        System.out.println(w2);
        System.out.println(w1);

        

//Пользователь вводит три строки, выведите в обратном порядке.
//2.

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!! 2 !!!!!!!!!!!!!!!!!!!!!!!!!!!");
        //хардкодинг чтобы не было пропуска ввода строки...
        w1=scanner.nextLine();
        w1=scanner.nextLine();
        
        w2=scanner.nextLine();
        w3=scanner.nextLine();
        
        System.out.println("вывод:");
        System.out.println(w3);
        System.out.println(w2);
        System.out.println(w1);



//Используя System.in (без сканнера), считайте слово из 5 букв
//3.

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!! 3 !!!!!!!!!!!!!!!!!!!!!!!!!!!");


        InputStream xx = System.in;
        String str=new String();
        
        while (str.length() < 5) {
        str+=(char)xx.read(); 
        }   
        System.out.println(str);


//Используя System.in (без сканнера),считайте слово до пробела. Макс 
//размер слова 10 символов.
//4.

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!! 4 !!!!!!!!!!!!!!!!!!!!!!!!!!!");


        InputStream xx2 = System.in;
        String str2=new String();

        while (str2.length() < 10) {
          char ch=(char)xx2.read();
          if (ch!=' ')
          {str2+=ch;} 
          else 
          {break;}
        }   
        System.out.println(str2);




//Используя Scanner на основе FileInputStream, прочесть из файла строку: 
//название другого файла. Из этог
//о другого файла прочесть название третьего 
//файла, и в третий файл записать букву, которую пользователь введет через 
//System.in (без сканера)
//5.
        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!! 5 !!!!!!!!!!!!!!!!!!!!!!!!!!!");

//        String path = new String();  
//        StringBuilder sb = new StringBuilder();
            String file1 = new String();
            String wrtFile = new String();
            
           
            
//        
//
//        path="/home/ramanzes/myapp/synergy/1_9/NetBeans/tema1_urok1_9/mavenproject1/src/main/java/com/mycompany/mavenproject1/";
//        //из этого файла считываем имя первого файла. т.е. newfile1.txt 
//        sb.append(path+"qwerty.txt");
//
//        InputStream stream = new FileInputStream(sb.toString());
//        Scanner filescan = new Scanner(stream);
//        //сбрасываем буфер для приёма значения новой строки
//        sb.setLength(0);
//
//        //обновим имя файла
//        sb.append(path+filescan.next());
//        //из этого файла считываем имя второго файла т.е. newfile2.txt в который ведём запись
//
//        
//        InputStream stream1 = new FileInputStream(sb.toString());
//        //сбрасываем буфер для приёма значения новой строки
//        sb.setLength(0);
//        
//        filescan = new Scanner(stream1);
//        sb.append(path+filescan.next());
//        fullname=sb.toString();
//        sb.setLength(0);


        //получаем имя первого файла из файла qwerty.txt без полного пути
        file1=getFilename("qwerty.txt",false);
        //получаем имя второго из имени полученного из файла, с полным путём чтобы сделать в него запись
        wrtFile=getFilename(file1,true);
        
        System.out.println("Введите символ, который запишется в файл "+wrtFile);
        char ch=(char)System.in.read();
        try {
            FileWriter writer = new FileWriter(wrtFile);
            writer.write(ch);
            writer.close();
        } catch (IOException e) {
            System.out.println(e);
        }
        


        
//Пользователь вводит 7символов. Используя System.in (без сканнера), 
//считайте целое число до первой нецифры.(пример: «
//35 руб», ответ 35.) считаем, 
//что первым пользователь всегда вводит цифру.
//6.


        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!! 6 !!!!!!!!!!!!!!!!!!!!!!!!!!!");


        InputStream xx6 = System.in;
        String str6=new String();

        while (str6.length() <= 7) {
            char ch6=(char)xx6.read();
//            System.out.println((int)ch6);

            //из предыдущего ввода остаётся символ 10 после Enter, просто игнорим его
          if (((int)ch6==10)||((int)ch6>=48)&&((int)ch6<=57)){
              if((int)ch6!=10)   
              str6+=ch6;
          } 
          else 
          {break;}
        }   
        System.out.println(str6);


        System.out.println("Аналогично предыдущей задаче, но пользователь всегда вводит дробное  число (пример ввода: «3.5 кг»)");
        


        String str6_1=new String();

        while (str6_1.length() <= 7) {
            char ch6_1=(char)xx6.read();
//            System.out.println((int)ch6_1);

            //из предыдущего ввода остаётся символ 10 после Enter, просто игнорим его
          if (((int)ch6_1==46)||((int)ch6_1==10)||((int)ch6_1>=48)&&((int)ch6_1<=57)){
              if((int)ch6_1!=10)   
              str6_1+=ch6_1;
          } 
          else 
          {break;}
        }   
        System.out.println(str6_1);






//Аналогично предыдущей задаче, но пользователь всегда вводит дробное 
//число (пример ввода: «3.5 кг»)


//7.
//Пользователь вводит 10 строк. Те из них, в которых есть 
//восклицательный знак, вывед
//ите в stderr. Попробуйте использовать цикл.

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!! 7 !!!!!!!!!!!!!!!!!!!!!!!!!!!");

        System.out.println("Пользователь вводит 10 строк. Те из них, в которых естьвосклицательный знак, выведите в stderr. Попробуйте использовать цикл.");

        //создаём пустой список строк
        List<String> myList = new ArrayList<>();
        
        for (int i = 0; i < 10; i++) {
            //заполняем список
            myList.add(ScannerStr.getStr());
        }
        
        System.out.println("Выводим строки в которых есть !");
        for (int i = 0; i < 10; i++) {
            ScannerStr.getV(myList.get(i));
        }


//8.
//Пользователь вводит 10 строк, потом число max. Вывести в stdout те 
//строки, длинна которых меньше max, а в stderr те, длинна которых больше, 
//обрезав по max
//

        System.out.println("!!!!!!!!!!!!!!!!!!!!!!!!!!!! 8 !!!!!!!!!!!!!!!!!!!!!!!!!!!");

        //создаём пустой список строк
        List<String> myList2 = new ArrayList<>();


       for (int i = 0; i < 10; i++) {
            //заполняем список
            myList2.add(ScannerStr.getStr());
        }

        System.out.println("Введите максимум Max = ");
        int max=scanner.nextInt();
        
        for (int i = 0; i < 10; i++) {
            System.out.println(ScannerStr.filterStr(myList2.get(i), max));
        }

    }   
    
    //делаем процедуру считывания имени нового файла из файла, через функцию, так как объекты строк являются неизменяемые в java

    /**
     *
     * @param file
     * @param fullpath
     * @return
     * @throws java.io.FileNotFoundException
     */
    public static String getFilename(String file, Boolean fullpath) throws FileNotFoundException {
        
        String path = new String();  
        StringBuilder sb = new StringBuilder();
        String fullname = new String();        

        path="/home/ramanzes/myapp/synergy/1_9/NetBeans/tema1_urok1_9/mavenproject1/src/main/java/com/mycompany/mavenproject1/";
        //из этого файла считываем имя первого файла. т.е. newfile1.txt 
        fullname=path+file;

        InputStream stream = new FileInputStream(fullname);
        
        Scanner filescan = new Scanner(stream);
        
        if (!fullpath)
            return (filescan.next());
        else
            return (path+filescan.next());
    }
    
}
